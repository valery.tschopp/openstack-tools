#!/usr/bin/env python3
#
#  Copyright (c) 2021 SWITCH http://www.switch.ch
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import sys
import argparse
import munch

# OpenStack SDK
# see https://developer.openstack.org/sdks/python/openstacksdk/users/index.html#api-documentation
import openstack
import openstack.exceptions

#
# Search for unsecure security group rules in all the user projects.
# Unsecure rule:
# - ingress
# - tcp/udp/all protocol (icmp is +/- secure)
# - all ports open
# - all IPv4/IPv6 address allowed
#
def main():

    parser = argparse.ArgumentParser(description='Scan for unsecure (all ports open to everyone) security group rules in user projects',
                                     epilog='The standard OS_* env variables are read to authenticate the user.')
    parser.add_argument('-v', '--verbose', help='verbose', action='store_true')
    parser.add_argument('-d', '--debug', help='debug', action='store_true')
    parser.add_argument('-r', '--region', help='only scan in REGION')
    project_group = parser.add_mutually_exclusive_group()
    project_group.add_argument('-m', '--match', help='Project name SUBSTRING to match')
    project_group.add_argument('-p', '--project', help='Project name or ID')
    args = parser.parse_args()

    # -d|--debug will log to stdout
    openstack.enable_logging(debug=args.debug, stream=sys.stdout)

    # read OS_* env variables
    auth_url = os.environ.get('OS_AUTH_URL')
    region_name = os.environ.get('OS_REGION_NAME')
    username = os.environ.get('OS_USERNAME')
    password = os.environ.get('OS_PASSWORD')
    project_name = os.environ.get('OS_PROJECT_NAME')
    project_domain_name = os.environ.get('OS_PROJECT_DOMAIN_NAME', 'Default')
    user_domain_name = os.environ.get('OS_USER_DOMAIN_NAME', 'Default')

    # first connection to check projects and available regions
    api = openstack.connect(load_envvars=False,
                            auth_url=auth_url,
                            region_name=region_name,
                            username=username, password=password,
                            project_name=project_name,
                            project_domain_name=project_domain_name,
                            user_domain_name=user_domain_name)

    # get the user's projects
    projects = api.identity.user_projects(user=api.current_user_id)

    if args.match:
        # filter project name by substring
        projects = filter(lambda p: args.match.lower() in p.name.lower(), projects)
    elif args.project:
        # filter project by name or id
        projects = filter(lambda p: args.project == p.name or args.project == p.id, projects)

    # generator to list (reusable)
    projects = list(projects)
    if not projects:
        print("No project found. Check your --project PROJECT or --match MATCH parameter")
        return 1

    # loop the available regions
    regions = api.identity.regions()
    if args.region:
        # filter region if -r REGION arg exists
        regions = filter(lambda r: r.id.upper() == args.region.upper(), regions)
    for region in regions:
        print("region:", region.id)
        for project in projects:
            if args.verbose:
                print(f"- project: {project.name} [{project.id}]")
            # reconnect to region/project
            api = openstack.connect(load_envvars=False,
                                    auth_url=auth_url,
                                    region_name=region.id,
                                    username=username, password=password,
                                    project_name=project.name,
                                    project_domain_name=project_domain_name,
                                    user_domain_name=user_domain_name)
            security_groups = api.list_security_groups()
            for group in security_groups:
                if args.verbose:
                    print(f"  - security group: {group.name} [{group.id}]")
                for rule in group.security_group_rules:
                    rule = munch.Munch(rule)
                    if args.verbose:
                        print(f"    - rule: {rule.id} {rule.direction} {rule.protocol} {rule.ethertype} {rule.remote_ip_prefix} {rule.port_range_min}:{rule.port_range_max} {rule.remote_group_id}")
                    # unsecure rule: ingress, not icmp
                    if rule.direction == 'ingress' and rule.protocol != 'icmp':
                        # unsecure rule: not port defined, no remote_group defined
                        if not rule.port_range_min and not rule.remote_group_id:
                            # unsecure rule: open to all IPv4 and IPv6 ranges
                            if rule.remote_ip_prefix == '0.0.0.0/0' or rule.remote_ip_prefix == '::/0':
                                print(f"- project: {project.name} [{project.id}]")
                                print(f"  - security group: {group.name} [{group.id}]")
                                print( "    WARNING: rule: {} {} {} {} {} {}:{} {}".format(
                                    rule.id, rule.direction,
                                    rule.protocol, rule.ethertype,
                                    rule.remote_ip_prefix,
                                    rule.port_range_min, rule.port_range_max,
                                    rule.remote_group_id
                                ))

if __name__ == '__main__':
    main()
