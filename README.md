# Openstack Tools

Tools for OpenStack for users.

## SWITCHengines API credentials

To use the scripts, you typically need an OpenStack API access. Therefore you need
to use your SWITCHengines API credentials as described the FAQ [Create SWITCHengines API Credentials](https://help.switch.ch/engines/faq/how-to-create-api-credentials/)

## Dependencies

The tools require `python3` (and `pip3`) and the OpenStack libraries.

```bash
pip3 install python-openstackclient python-neutronclient munch
```

## security-groups-scan-as_user.py

Search for unsecure security group rules in all the user projects, in all regions.

### Unsecure Security Group Rules

A security group rule is **UNSECURE** if:

- direction: `ingress`
- protocol: `None` or `tcp` or `udp` (`None` is ALL protocols, `icmp` is more or less secure)
- port: `None` (all ports are open)
- remote_ip_prefix: `0.0.0.0/0` or `::/0` (all IPv4/IPv6 address allowed)

### Usage

```bash
usage: security-groups-scan-as_user.py [-h] [-v] [-d] [-r REGION] [-m MATCH | -p PROJECT]

Scan for unsecure (all ports open to everyone) security group rules in user projects

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         verbose
  -d, --debug           debug
  -r REGION, --region REGION
                        only scan in REGION
  -m MATCH, --match MATCH
                        Project name SUBSTRING to match
  -p PROJECT, --project PROJECT
                        Project name or ID

The standard OS_* env variables are read to authenticate the user.
```
